# LO-ZAR
A script for easily researching yourself (or an individual) online.

# How To Use:
1. cd LO-ZAR
2. chmod +x *.sh
3. ./startLO-ZAR.sh 

# Disclaimer
LO-ZAR is for research and educational purposes only
